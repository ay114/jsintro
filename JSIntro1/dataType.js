/*DataType exercise
In this exercise, we are going to create our own superhero character!
Remember to use console.log to help you debug*/

//First, declare an empty OBJECT variable called 'superhero'

let firstName = "Quick";
let lastName = "Hand";
let fullName = firstName + lastName;

let superhero = {
	firstName: "Quick",
	lastName: "Hand",
	fullName: fullName,
	HP: 100,
	strengths: ["Rock", "Paper", "Scissors"],
	weaknesses: ["Paper", "Scissors", "Rock"],
	motto: "I challenge you"
};

/*Give the 'superhero' a first name, last name, superhero name, HP(hit points), 
3 strengths in an ARRAY, 3 weaknesses in an ARRAY, a motto */
//let firstName = "Quick";
//let lastName = "Hand";
//let fullName = firstName + lastName;
//let HP = 100;
//let strengths = ["Rock", "Paper", "Scissors"];
//let weaknesses = ["Paper", "Scissors", "Rock"];
//let motto = "I challenge you";

// superhero.firstName = firstName;
//console.log(superhero);

//Set the superhero's alive status to true
superhero.status = true;


/*Now console.log to introduce the character using the 'superhero' OBJECT
For example, 'Hi, I'm Quick Hands. Fear me! 
My strengths are Rock, Paper and Scissors. My weaknesses are Paper, Scissors and Rock.' */
let intro = "Hi, I'm";

let strengthOne = superhero.strengths[0];
let strengthTwo = superhero.strengths[1];
let strengthThree = superhero.strengths[2];
let weaknessOne = superhero.weaknesses[0];
let weaknessTwo = superhero.weaknesses[1];
let weaknessThree = superhero.weaknesses[2];

console.log(intro, superhero.fullName, ".", superhero.motto, "strengths", strengthOne, strengthTwo, "and", strengthThree, "weakness", weaknessOne, weaknessTwo, "and", weaknessThree);

console.log(intro, superhero.fullName + "."); // string concatenation 

console.log(`Hi, I'm ${superhero.fullName}.`); // string interpolation


/*One day, the superhero is on the way to rescue a kitten on the tree, a monster shows up to fight.
Create 4 BOOLEAN variables: hero, monster, hit, miss, and assign them the correct value so that:
superhero and monster cannot both win; either superhero or monster wins;
superhero always hits and never miss; 
monster always misses and never hit.
At the end, console.log superhero's victory.*/
let hero = true;
let monster = false;
let hit = true;
let miss = false;
let result;

if (hero | monster){
	result = "Superhero wins";
} else {
	result = "Monster wins";
}
//console.log(hero && (hit | miss));
console.log(hero && miss | hit);
console.log(true | miss);
if (hero && hit | miss){
	result = "Superhero hits";
} else {
	result = "Superhero hits";
}

//console.log("Did the superhero win?", result);
console.log("Did the superhero hit?", result);


/*The superhero trips over a cardboard box, and loses half of the HP,
loses all the strengths and gains a weakness: cardboard box.
console.log the new superhero status and make sure the hero is still alive.*/

//superhero.HP = superhero.HP - 50;
//superhero.HP -= 50;

superhero.HP = superhero.HP * 0.5;
superhero.HP *= 0.5;

console.log(superhero.HP);

superhero.weaknesses.push("cardboard box");

console.log(superhero.weaknesses);

/*BONUS: create a monster character. 
Give both the monster and the hero attack and defense value. 
Now let them battle till the end. The hero might die this time ;(
*/